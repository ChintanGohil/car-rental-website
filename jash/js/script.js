/**
 * scripts.js
 * Contains Script for basic static website named "White Graphics"
 */
$(window).on('load',function(){
    $("#preloader").delay(4500).fadeOut("slow");
});

$(document).ready(function(){
    $("#team-right").owlCarousel({
        items:2,
        autoplaytrue: true,
        margin: 20,
        loop: true,
        nav: true,
        smartSpeed: 700,
        autoplayHoverPause: true,
        dots: false,
        navText: ['<i class= "lni-chevron-left-circle"></i>', '<i class= "lni-chevron-right-circle"></i>']
    });
});

$(document).ready(function(){
    $("#progress-elements").waypoint(function(){
        $(".progress-bar").each(function(){
            $(this).animate({
                width: $(this).attr("aria-valuenow")+"%"
            },800);
        });
        this.destroy();
    },{
        offset: 'bottom-in-view'
    });
});

$(document).ready(function(){
    $('#services-tabs').responsiveTabs({
        animation: 'slide'
    });
});

$(document).ready(function(){
    $('#isotope-container').isotope({});
    $('#isotope-filters').on("click","button",function(){
        let filterValue=$(this).attr("data-filter");
        $('#isotope-container').isotope({
            filter: filterValue
        });
        $('#isotope-filters').find('.active').removeClass('active');
        $(this).addClass('active');
    });
});


//script for magnific popup
$(document).ready(function(){
    $("#portfolio-wrapper").magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300,
            easing: 'ease-in-out',
            opener: function(openerElement){
                return openerElement.is('img') ? openerElement :
                openerElement.find('img');
            }
        }
    });
});
$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items: 1,
        autoplay:true,
        margin:20,
        loop:true,
        nav:true,
        smartSpeed:700,
        autoplayHoverPause:true,
        dots:false,
        navText:['<i class= "lni-chevron-left-circle"></i>', '<i class= "lni-chevron-right-circle"></i>']
    });
});



$(function () {
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });
});

$(document).ready(function(){
    $("#client-names").owlCarousel({
        items:6,
        autoplay:true,
        smartSpeed:700,
        dots:false,
        nav:true,
        autoplayHoverPause:true,
        loop:true,
        margin:20,
        navText:['<i class= "lni-chevron-left-circle"></i>', '<i class= "lni-chevron-right-circle"></i>'],
        responsive:{
            0:{
                items:2
            },
            480:{
                items:3,
                nav:false
            },
            786:{
                items:6,
            }
        }
    })
})












