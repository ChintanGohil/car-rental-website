$(document).ready(function(){
    $("#showcase").owlCarousel({
        items:1,
        autoplaytrue: true,
        margin: 20,
        loop: true,
        nav: true,
        smartSpeed: 700,
        autoplayHoverPause: true,
        dots: false,
        navText:['<i class="lni lni-chevron-left"></i>', '<i class="lni lni-chevron-right"></i>']
    });
});

$(function(){
    $(".hamburger").click(function(){
        $("#mobile-menu").slideDown();
        $("#mobile-header").css("display","none");
        $(".collection").css("display","none");
    });
    $(".cross").click(function(){
        $("#mobile-menu").slideUp();
        $("#mobile-header").slideDown();
        $(".collection").slideDown();
    })
});

$(function(){
    navOperation();
    console.log("hello1");
    $(window).scroll(function(){
        navOperation();
        console.log($(window).scrollTop());
    });
    function navOperation(){
        if($(window).scrollTop()>50){
            $(".logo").css('display',"none");
            $(".logo-s").css('display',"block");
//            $(".menu-list").addClass("stickeyy");
            $(".menu-list").css("position","fixed");
        }else{
            $(".logo").css('display',"block");
            $(".logo-s").css('display',"none");
            $(".menu-list").css("position","relative");
        }
    }
});